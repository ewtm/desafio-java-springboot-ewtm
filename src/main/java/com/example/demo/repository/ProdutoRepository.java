package com.example.demo.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Produtos;

public interface ProdutoRepository extends JpaRepository<Produtos, Integer> {
	
	@Query("SELECT p FROM Produtos p WHERE p.price > :min_price OR p.price < :max_price"
			+ " OR p.name like :q  OR p.description like :q " )
	List<Produtos> findByNameOrPrice(
			@Param("min_price") BigDecimal min_price,
			@Param("max_price") BigDecimal max_price,
			@Param("q") String q
			);

}
