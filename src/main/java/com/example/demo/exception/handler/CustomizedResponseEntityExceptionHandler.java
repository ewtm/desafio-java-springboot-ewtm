package com.example.demo.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException.BadRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.exception.StandardError;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler  {
	
	@ExceptionHandler(ResourceNotFoundException.class)
	public final ResponseEntity<StandardError> handledResourceNotFoundException(Exception ex, WebRequest request){
		StandardError err = new StandardError(HttpStatus.NOT_FOUND.value(), ex.getMessage());
		return new ResponseEntity<>(err, HttpStatus.NOT_FOUND);
		
	}
	
	@ExceptionHandler(BadRequestException.class)
	public final ResponseEntity<StandardError> handledBadRequestException(Exception ex, WebRequest request){
		StandardError err = new StandardError(HttpStatus.BAD_REQUEST.value(), ex.getMessage());
		return new ResponseEntity<>(err, HttpStatus.BAD_REQUEST);
		
	}

}
