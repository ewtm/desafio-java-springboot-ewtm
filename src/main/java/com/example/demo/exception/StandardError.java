package com.example.demo.exception;

import java.io.Serializable;

public class StandardError  implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private Integer status_code;
	private String message ;
	
	
	
	public StandardError(Integer status_code, String message) {
		super();
		this.status_code = status_code;
		this.message = message;
	}
	
	public Integer getStatusCode() {
		return status_code;
	}
	public void setStatusCode(Integer status_code) {
		this.status_code = status_code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
	

}
