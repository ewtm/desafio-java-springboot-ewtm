package com.example.demo.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Produtos;
import com.example.demo.model.ProdutosDTO;
import com.example.demo.repository.ProdutoRepository;


@Service
public class ProdutoService {
	
	@Autowired
	private ProdutoRepository repo;
	
	public Produtos insert(Produtos obj) {	
			
		obj.setId(null);	
		ValidationField(obj);
		return repo.save(obj);	
			
	}
	
	public List<Produtos> findAll(){
		return repo.findAll();
	}
	
	
	
	public Produtos findById(Integer id) {
		Optional<Produtos> obj = repo.findById(id);
		return obj.orElseThrow( () -> new ResourceNotFoundException("No record found for this ID") );
	}
	
	public Produtos update (Produtos obj) {
		
		Produtos newProduct = repo.findById(obj.getId()).orElseThrow(() -> new ResourceNotFoundException("No record found for this ID"));
		newProduct.setName(obj.getName());
		newProduct.setPrice(obj.getPrice());
		newProduct.setDescription(obj.getDescription());
		
		return repo.save(newProduct);
	}
	
	public void delete(Integer id) {
		Produtos entity = repo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("No record found for this ID"));
		repo.delete(entity);
	}
	
	
	
	
	public Produtos fromDTO (ProdutosDTO objDTO) {
		return new Produtos(objDTO.getId(),objDTO.getName(),objDTO.getPrice(),objDTO.getDescription());
	}
	
	public void ValidationField(Produtos obj) {
		if(obj.getDescription() == null ) {
			throw new BadRequestException("Description null");
		} else if (obj.getName() == null ) {
			throw new BadRequestException("Name null");
		} else if (obj.getPrice() == null ) {
			throw new BadRequestException("Price null");
		}
	}

	public List<Produtos> search(BigDecimal min_price, BigDecimal max_price,String q) {
		return repo.findByNameOrPrice(min_price,max_price,"%"+q+"%");
	}

	
	

	
	
	
}
