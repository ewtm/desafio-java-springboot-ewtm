package com.example.demo.controller;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Produtos;
import com.example.demo.model.ProdutosDTO;
import com.example.demo.service.ProdutoService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProdutoController {

	@Autowired
	private ProdutoService service;
	
	@PostMapping
	public Produtos insert(@RequestBody Produtos produtos) {			
		return service.insert(produtos);	
		
	}
	
	@GetMapping
	public List<Produtos> findAll(){
		return service.findAll();
	}
	
	@GetMapping("/{id}")
	public Produtos  findById(@PathVariable("id") Integer id){
		return service.findById(id);
	}
	
	@PutMapping("/{id}")
	public Produtos update(@PathVariable Integer id, @RequestBody ProdutosDTO produto) {
		Produtos obj = service.fromDTO(produto);
		obj.setId(id);
		return service.update(obj);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable("id") Integer id){
		service.delete(id);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/search")
	public List<Produtos> search(
			@RequestParam(value="min_price",required = false) BigDecimal min_price,
			@RequestParam(value="max_price",required = false) BigDecimal max_price,
			@RequestParam(value="q",required = false) String q
			){
		return service.search(min_price,max_price,q);
	}
	
	
	/*
	 * 	@RequestParam(value="min_price",required = false) Integer min_price,
			@RequestParam(value="max_price",required = false) Integer max_price
	 * 
	 * 
	 */
	 
	
	
}
